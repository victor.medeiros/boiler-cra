# boiler-cra
cra-jest-storybook-sass-pwa-atomic-redux

##yarn
"start": "react-scripts start"
"build": "sass src/styles/scss:src/styles/css & react-scripts build"
"test": "react-scripts test"

##yarn run
"eject": "react-scripts eject"
"sass": "sass --watch src/styles/scss:src/styles/css"
"storybook": "start-storybook -p 9009 -s public"
"build-storybook": "build-storybook -c .storybook"
"chromatic": "CHROMATIC_APP_CODE=05v1u9qrunjq chromatic test --do-not-start"
