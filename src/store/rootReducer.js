import { combineReducers } from 'redux';
import tasks from '../components/micro/Task/reducer';

export default combineReducers({ tasks });
